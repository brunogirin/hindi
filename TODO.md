# To-do list

## State machine

- Refactor state machine to handle sub-machines and to make construction easier
- Make events from child to parent machine easier
- Handle situations where a parent state machine exits from a child state
  machine, either as a result of the parent or the child
- On close of the syllable machine, ensure that the current syllable has a
  vowel or a virama on last consonant and add the 'a' vowel if required
- Add the ability to roll back a state machine step by step (delete key)

## Keyboard and keymap

- Add special keys such as space and backspace
- Allow the alphabet to specify whether any special keys should be added
  to the keyboard
- Add a backspace key

## Overall app

- Add a translitation option both forward and backward
- Support multiple screen sizes better (the current one is hard-coded to work
  OK on a Huawei P9 or larger)
