import { shallowMount } from '@vue/test-utils'
import Key from '@/components/Key.vue'
import Letter from '@/components/Letter.js'

describe('Key.vue', () => {
  it('renders glyph when passed a letter', () => {
    const letter = new Letter('vowel', {independent: 'अ', dependent: '', latin: 'a'}, 'independent');
    const initials = ['consonant', 'vowel'];
    const wrapper = shallowMount(Key, {
      propsData: { letter, initials }
    })
    expect(wrapper.text()).toMatch('अ')
  });

  it('renders nothing when passed nothing', () => {
    const wrapper = shallowMount(Key, {
      propsData: {}
    })
    expect(wrapper.text()).toMatch('')
  });
})
