import Devanagari from '@/components/Devanagari.js'
import Letter from '@/components/Letter.js'

describe('Devanagari.js', () => {
  it('keymap.columnCount', () => {
    const alphabet = new Devanagari();
    expect(alphabet.keymap.columnCount).toBe(10);
  });

  it('keymap.rowCount', () => {
    const alphabet = new Devanagari();
    expect(alphabet.keymap.rowCount).toBe(6);
  });

  // alphabet.a

  if('a.glyph independent', () => {
    const alphabet = new Devanagari();
    expect(alphabet.a.glyph('independent')).toBe('अ');
  })

  if('a.glyph dependent', () => {
    const alphabet = new Devanagari();
    expect(alphabet.a.glyph('dependent')).toBe('');
  })

  // ===========================================================================
  // syllable state machine

  it('syllable_state_machine.process consonant', () => {
    const alphabet = new Devanagari();
    const state_machine = alphabet.syllable_state_machine;
    expect(state_machine.state.state).toBe('start');
    expect(state_machine.state.data.text).toBe('');
    expect(state_machine.state.data.letters).toEqual([]);
    expect(state_machine.state.data.glyphs).toEqual([]);
    const consonant = new Letter('consonant', {independent: 'क', latin: 'k'}, 'independent');
    const result = state_machine.process(consonant);
    expect(result).toBe(null);
    expect(state_machine.state.state).toBe('consonant');
    expect(state_machine.state.data.text).toBe('क');
    expect(state_machine.state.data.letters).toEqual([consonant]);
    expect(state_machine.state.data.glyphs).toEqual(['क']);
    expect(state_machine.data()).toEqual({
        highlight: 'क',
        display_hint: {
            prefix: 'क',
            categories: ['vowel', 'nukta', 'virama']
        }
    });
  });

  it('syllable_state_machine.process vowel', () => {
    const alphabet = new Devanagari();
    const state_machine = alphabet.syllable_state_machine;
    expect(state_machine.state.state).toBe('start');
    expect(state_machine.state.data.text).toBe('');
    expect(state_machine.state.data.letters).toEqual([]);
    expect(state_machine.state.data.glyphs).toEqual([]);
    const vowel = new Letter('vowel', {independent: 'आ', dependent: 'ा', latin: 'ā'}, 'independent');
    const result = state_machine.process(vowel);
    expect(result).toBe(null);
    expect(state_machine.state.state).toBe('vowel');
    expect(state_machine.state.data.text).toBe('आ');
    expect(state_machine.state.data.letters).toEqual([vowel]);
    expect(state_machine.state.data.glyphs).toEqual(['आ']);
    expect(state_machine.data()).toEqual({
        highlight: 'आ',
        display_hint: {
            prefix: 'आ',
            categories: ['bindu']
        }
    });
  });

  it('syllable_state_machine.process consonant + vowel', () => {
    const alphabet = new Devanagari();
    const state_machine = alphabet.syllable_state_machine;
    const consonant = new Letter('consonant', {independent: 'क', latin: 'k'}, 'independent');
    const r1 = state_machine.process(consonant);
    expect(r1).toBe(null);
    const vowel = new Letter('vowel', {independent: 'आ', dependent: 'ा', latin: 'ā'}, 'independent');
    const r2 = state_machine.process(vowel);
    expect(r2).toBe(null);
    expect(state_machine.state.state).toBe('vowel');
    expect(state_machine.state.data.text).toBe('का');
    expect(state_machine.state.data.letters).toEqual([consonant, vowel]);
    expect(state_machine.state.data.glyphs).toEqual(['क', 'ा']);
    expect(state_machine.data()).toEqual({
        highlight: 'का',
        display_hint: {
            prefix: 'का',
            categories: ['bindu']
        }
    });
  });

  it('syllable_state_machine.process consonant + nukta', () => {
    const alphabet = new Devanagari();
    const state_machine = alphabet.syllable_state_machine;
    const consonant = new Letter('consonant', {independent: 'क', latin: 'k'}, 'independent');
    const r1 = state_machine.process(consonant);
    expect(r1).toBe(null);
    const nukta = new Letter('nukta', {dependent: '़'}, 'dependent');
    const r2 = state_machine.process(nukta);
    expect(r2).toBe(null);
    expect(state_machine.state.state).toBe('nukta');
    expect(state_machine.state.data.text).toBe('क़');
    expect(state_machine.state.data.letters).toEqual([consonant, nukta]);
    expect(state_machine.state.data.glyphs).toEqual(['क', '़']);
    expect(state_machine.data()).toEqual({
        highlight: 'क़',
        display_hint: {
            prefix: 'क़',
            categories: ['vowel', 'virama']
        }
    });
  });

  it('syllable_state_machine.process consonant + nukta + vowel', () => {
    const alphabet = new Devanagari();
    const state_machine = alphabet.syllable_state_machine;
    const consonant = new Letter('consonant', {independent: 'क', latin: 'k'}, 'independent');
    const r1 = state_machine.process(consonant);
    expect(r1).toBe(null);
    const nukta = new Letter('nukta', {dependent: '़'}, 'dependent');
    const r2 = state_machine.process(nukta);
    expect(r2).toBe(null);
    const vowel = new Letter('vowel', {independent: 'आ', dependent: 'ा', latin: 'ā'}, 'independent');
    const r3 = state_machine.process(vowel);
    expect(r3).toBe(null);
    expect(state_machine.state.state).toBe('vowel');
    expect(state_machine.state.data.text).toBe('क़ा');
    expect(state_machine.state.data.letters).toEqual([consonant, nukta, vowel]);
    expect(state_machine.state.data.glyphs).toEqual(['क', '़', 'ा']);
    expect(state_machine.data()).toEqual({
        highlight: 'क़ा',
        display_hint: {
            prefix: 'क़ा',
            categories: ['bindu']
        }
    });
  });

  it('syllable_state_machine.process consonant + virama', () => {
    const alphabet = new Devanagari();
    const state_machine = alphabet.syllable_state_machine;
    const consonant = new Letter('consonant', {independent: 'क', latin: 'k'}, 'independent');
    const r1 = state_machine.process(consonant);
    expect(r1).toBe(null);
    const virama = new Letter('virama', {dependent: '्'}, 'dependent');
    const r2 = state_machine.process(virama);
    expect(r2).toBe(null);
    expect(state_machine.state.state).toBe('virama');
    expect(state_machine.state.data.text).toBe('क्');
    expect(state_machine.state.data.letters).toEqual([consonant, virama]);
    expect(state_machine.state.data.glyphs).toEqual(['क', '्']);
    expect(state_machine.data()).toEqual({
        highlight: 'क्',
        display_hint: null
    });
  });

  it('syllable_state_machine.process consonant + virama + consonant (adjunct consonant)', () => {
    const alphabet = new Devanagari();
    const state_machine = alphabet.syllable_state_machine;
    const consonant = new Letter('consonant', {independent: 'स', latin: 's'}, 'independent');
    const r1 = state_machine.process(consonant);
    expect(r1).toBe(null);
    const virama = new Letter('virama', {dependent: '्'}, 'dependent');
    const r2 = state_machine.process(virama);
    expect(r2).toBe(null);
    const consonant2 = new Letter('consonant', {independent: 'त', latin: 't'}, 'independent')
    const r3 = state_machine.process(consonant2);
    expect(r3).toBe(null);
    expect(state_machine.state.state).toBe('consonant');
    expect(state_machine.state.data.text).toBe('स्त');
    expect(state_machine.state.data.letters).toEqual([consonant, virama, consonant2]);
    expect(state_machine.state.data.glyphs).toEqual(['स', '्', 'त']);
    expect(state_machine.data()).toEqual({
        highlight: 'स्त',
        display_hint: {
            prefix: 'स्त',
            categories: ['vowel', 'nukta', 'virama']
        }
    });
  });

  it('syllable_state_machine.process consonant + nukta + virama', () => {
    const alphabet = new Devanagari();
    const state_machine = alphabet.syllable_state_machine;
    const consonant = new Letter('consonant', {independent: 'क', latin: 'k'}, 'independent');
    const r1 = state_machine.process(consonant);
    expect(r1).toBe(null);
    const nukta = new Letter('nukta', {dependent: '़'}, 'dependent');
    const r2 = state_machine.process(nukta);
    expect(r2).toBe(null);
    const virama = new Letter('virama', {dependent: '्'}, 'dependent');
    const r3 = state_machine.process(virama);
    expect(r3).toBe(null);
    expect(state_machine.state.state).toBe('virama');
    expect(state_machine.state.data.text).toBe('क़्');
    expect(state_machine.state.data.letters).toEqual([consonant, nukta, virama]);
    expect(state_machine.state.data.glyphs).toEqual(['क', '़', '्']);
    expect(state_machine.data()).toEqual({
        highlight: 'क़्',
        display_hint: null
    });
  });

  it('syllable_state_machine.process consonant + vowel + bindu', () => {
    const alphabet = new Devanagari();
    const state_machine = alphabet.syllable_state_machine;
    const consonant = new Letter('consonant', {independent: 'क', latin: 'k'}, 'independent');
    const r1 = state_machine.process(consonant);
    expect(r1).toBe(null);
    const vowel = new Letter('vowel', {independent: 'आ', dependent: 'ा', latin: 'ā'}, 'independent');
    const r2 = state_machine.process(vowel);
    expect(r2).toBe(null);
    const bindu = new Letter('bindu', {dependent: 'ं'}, 'dependent');
    const r3 = state_machine.process(bindu);
    expect(r3).toEqual({
        category: 'closed',
        syllable: {
            text: 'कां',
            letters: [consonant, vowel, bindu],
            glyphs: ['क', 'ा', 'ं']
        }
    });
    expect(state_machine.state.state).toBe('end');
    expect(state_machine.state.data.text).toBe('कां');
    expect(state_machine.state.data.letters).toEqual([consonant, vowel, bindu]);
    expect(state_machine.state.data.glyphs).toEqual(['क', 'ा', 'ं']);
    expect(state_machine.data()).toEqual({
        highlight: 'कां',
        display_hint: null
    });
  });

  it('syllable_state_machine.process consonant + nukta + vowel + bindu', () => {
    const alphabet = new Devanagari();
    const state_machine = alphabet.syllable_state_machine;
    const consonant = new Letter('consonant', {independent: 'क', latin: 'k'}, 'independent');
    const r1 = state_machine.process(consonant);
    expect(r1).toBe(null);
    const nukta = new Letter('nukta', {dependent: '़'}, 'dependent');
    const r2 = state_machine.process(nukta);
    expect(r2).toBe(null);
    const vowel = new Letter('vowel', {independent: 'आ', dependent: 'ा', latin: 'ā'}, 'independent');
    const r3 = state_machine.process(vowel);
    expect(r3).toBe(null);
    const bindu = new Letter('bindu', {dependent: 'ं'}, 'dependent');
    const r4 = state_machine.process(bindu);
    expect(r4).toEqual({
        category: 'closed',
        syllable: {
            text: 'क़ां',
            letters: [consonant, nukta, vowel, bindu],
            glyphs: ['क', '़', 'ा', 'ं']
        }
    });
    expect(state_machine.state.state).toBe('end');
    expect(state_machine.state.data.text).toBe('क़ां');
    expect(state_machine.state.data.letters).toEqual([consonant, nukta, vowel, bindu]);
    expect(state_machine.state.data.glyphs).toEqual(['क', '़', 'ा', 'ं']);
    expect(state_machine.data()).toEqual({
        highlight: 'क़ां',
        display_hint: null
    });
  });

  it('syllable_state_machine.process consonant + consonant', () => {
    const alphabet = new Devanagari();
    const state_machine = alphabet.syllable_state_machine;
    const consonant = new Letter('consonant', {independent: 'क', latin: 'k'}, 'independent');
    const r1 = state_machine.process(consonant);
    expect(r1).toBe(null);
    const consonant2 = new Letter('consonant', {independent: 'ख', latin: 'kh'}, 'independent');
    const r2 = state_machine.process(consonant2);
    expect(r2).toEqual({
        category: 'closed_restarted',
        syllable: {
            text: 'क',
            letters: [consonant, alphabet.a],
            glyphs: ['क', '']
        },
        letter: consonant2
    });
    expect(state_machine.state.state).toBe('end');
    expect(state_machine.state.data.text).toBe('क');
    expect(state_machine.state.data.letters).toEqual([consonant, alphabet.a]);
    expect(state_machine.state.data.glyphs).toEqual(['क', '']);
  });

  it('syllable_state_machine.process consonant + nukta + consonant', () => {
    const alphabet = new Devanagari();
    const state_machine = alphabet.syllable_state_machine;
    const consonant = new Letter('consonant', {independent: 'क', latin: 'k'}, 'independent');
    const r1 = state_machine.process(consonant);
    expect(r1).toBe(null);
    const nukta = new Letter('nukta', {dependent: '़'}, 'dependent');
    const r2 = state_machine.process(nukta);
    expect(r2).toBe(null);
    const consonant2 = new Letter('consonant', {independent: 'ख', latin: 'kh'}, 'independent');
    const r3 = state_machine.process(consonant2);
    expect(r3).toEqual({
        category: 'closed_restarted',
        syllable: {
            text: 'क़',
            letters: [consonant, nukta, alphabet.a],
            glyphs: ['क', '़', '']
        },
        letter: consonant2
    });
    expect(state_machine.state.state).toBe('end');
    expect(state_machine.state.data.text).toBe('क़');
    expect(state_machine.state.data.letters).toEqual([consonant, nukta, alphabet.a]);
    expect(state_machine.state.data.glyphs).toEqual(['क', '़', '']);
  });

  it('syllable_state_machine.process consonant + vowel + consonant', () => {
    const alphabet = new Devanagari();
    const state_machine = alphabet.syllable_state_machine;
    const consonant = new Letter('consonant', {independent: 'क', latin: 'k'}, 'independent');
    const r1 = state_machine.process(consonant);
    expect(r1).toBe(null);
    const vowel = new Letter('vowel', {independent: 'आ', dependent: 'ा', latin: 'ā'}, 'independent');
    const r2 = state_machine.process(vowel);
    expect(r2).toBe(null);
    const consonant2 = new Letter('consonant', {independent: 'ख', latin: 'kh'}, 'independent');
    const r3 = state_machine.process(consonant2);
    expect(r3).toEqual({
        category: 'closed_restarted',
        syllable: {
            text: 'का',
            letters: [consonant, vowel],
            glyphs: ['क', 'ा']
        },
        letter: consonant2
    });
    expect(state_machine.state.state).toBe('end');
    expect(state_machine.state.data.text).toBe('का');
    expect(state_machine.state.data.letters).toEqual([consonant, vowel]);
    expect(state_machine.state.data.glyphs).toEqual(['क', 'ा']);
  });

  it('syllable_state_machine.process consonant + vowel + vowel', () => {
    const alphabet = new Devanagari();
    const state_machine = alphabet.syllable_state_machine;
    const consonant = new Letter('consonant', {independent: 'क', latin: 'k'}, 'independent');
    const r1 = state_machine.process(consonant);
    expect(r1).toBe(null);
    const vowel = new Letter('vowel', {independent: 'आ', dependent: 'ा', latin: 'ā'}, 'independent');
    const r2 = state_machine.process(vowel);
    expect(r2).toBe(null);
    const vowel2 = new Letter('vowel', {independent: 'इ', dependent: 'ी', latin: 'ī'}, 'independent');
    const r3 = state_machine.process(vowel2);
    expect(r3).toEqual({
        category: 'closed_restarted',
        syllable: {
            text: 'का',
            letters: [consonant, vowel],
            glyphs: ['क', 'ा']
        },
        letter: vowel2
    });
    expect(state_machine.state.state).toBe('end');
    expect(state_machine.state.data.text).toBe('का');
    expect(state_machine.state.data.letters).toEqual([consonant, vowel]);
    expect(state_machine.state.data.glyphs).toEqual(['क', 'ा']);
  });

  // ===========================================================================
  // main state machine

  it('state_machine.process consonant', () => {
    const alphabet = new Devanagari();
    const state_machine = alphabet.state_machine;
    expect(state_machine.state.state).toBe('between');
    expect(state_machine.state.data.words).toEqual([]);
    expect(state_machine.state.data.syllable_state_machine.state.state).toBe('start');
    const consonant = new Letter('consonant', {independent: 'क', latin: 'k'}, 'independent');
    const r1 = state_machine.process(consonant);
    expect(r1).toBe(null);
    expect(state_machine.state.state).toBe('in_syllable');
    expect(state_machine.state.data.words).toEqual([]);
    expect(state_machine.state.data.syllable_state_machine.state.state).toBe('consonant');
    expect(state_machine.state.data.syllable_state_machine.state.data.text).toBe('क');
    expect(state_machine.state.data.syllable_state_machine.state.data.letters).toEqual([consonant]);
    expect(state_machine.state.data.syllable_state_machine.state.data.glyphs).toEqual(['क']);
    expect(state_machine.initials()).toEqual(['closed_restarted', 'closed', 'space', 'vowel', 'nukta', 'virama', 'consonant']);
  });

  it('state_machine.process consonant + virama', () => {
    const alphabet = new Devanagari();
    const state_machine = alphabet.state_machine;
    expect(state_machine.state.state).toBe('between');
    expect(state_machine.state.data.words).toEqual([]);
    expect(state_machine.state.data.syllable_state_machine.state.state).toBe('start');
    const consonant = new Letter('consonant', {independent: 'क', latin: 'k'}, 'independent');
    const r1 = state_machine.process(consonant);
    expect(r1).toBe(null);
    const virama = new Letter('virama', {dependent: '्'}, 'dependent');
    const r2 = state_machine.process(virama);
    expect(r2).toBe(null);
    expect(state_machine.state.state).toBe('in_syllable');
    expect(state_machine.state.data.words).toEqual([]);
    expect(state_machine.state.data.syllable_state_machine.state.state).toBe('virama');
    expect(state_machine.state.data.syllable_state_machine.state.data.text).toBe('क्');
    expect(state_machine.state.data.syllable_state_machine.state.data.letters).toEqual([consonant, virama]);
    expect(state_machine.state.data.syllable_state_machine.state.data.glyphs).toEqual(['क', '्']);
    expect(state_machine.initials()).toEqual(['closed_restarted', 'closed', 'space', 'consonant', 'vowel']);
  });

  it('state_machine.process consonant + virama + consonant (conjunct consonant)', () => {
    const alphabet = new Devanagari();
    const state_machine = alphabet.state_machine;
    expect(state_machine.state.state).toBe('between');
    expect(state_machine.state.data.words).toEqual([]);
    expect(state_machine.state.data.syllable_state_machine.state.state).toBe('start');
    const consonant = new Letter('consonant', {independent: 'स', latin: 's'}, 'independent');
    const r1 = state_machine.process(consonant);
    expect(r1).toBe(null);
    const virama = new Letter('virama', {dependent: '्'}, 'dependent');
    const r2 = state_machine.process(virama);
    expect(r2).toBe(null);
    const consonant2 = new Letter('consonant', {independent: 'त', latin: 't'}, 'independent');
    const r3 = state_machine.process(consonant2);
    expect(r3).toBe(null);
    expect(state_machine.state.state).toBe('in_syllable');
    expect(state_machine.state.data.words).toEqual([]);
    expect(state_machine.state.data.syllable_state_machine.state.state).toBe('consonant');
    expect(state_machine.state.data.syllable_state_machine.state.data.text).toBe('स्त');
    expect(state_machine.state.data.syllable_state_machine.state.data.letters).toEqual([consonant, virama, consonant2]);
    expect(state_machine.state.data.syllable_state_machine.state.data.glyphs).toEqual(['स','्','त']);
    expect(state_machine.initials()).toEqual(['closed_restarted', 'closed', 'space', 'vowel', 'nukta', 'virama', 'consonant']);
  });

  it('state_machine.process consonant + virama + space (new word)', () => {
    const alphabet = new Devanagari();
    const state_machine = alphabet.state_machine;
    expect(state_machine.state.state).toBe('between');
    expect(state_machine.state.data.words).toEqual([]);
    expect(state_machine.state.data.syllable_state_machine.state.state).toBe('start');
    const consonant = new Letter('consonant', {independent: 'क', latin: 'k'}, 'independent');
    const r1 = state_machine.process(consonant);
    expect(r1).toBe(null);
    const virama = new Letter('virama', {dependent: '्'}, 'dependent');
    const r2 = state_machine.process(virama);
    expect(r2).toBe(null);
    const space = new Letter('space', {independent: ' ', latin: ' '}, 'independent');
    const r3 = state_machine.process(space);
    expect(r3).toBe(null);
    expect(state_machine.state.state).toBe('between');
    expect(state_machine.state.data.words).toEqual([
        {
            syllables: [{
                text: 'क्',
                letters: [consonant, virama],
                glyphs: ['क', '्']
            }]
        },{
            syllables: []
        }
    ]);
    expect(state_machine.state.data.syllable_state_machine.state.state).toBe('start');
    expect(state_machine.state.data.syllable_state_machine.state.data.text).toBe('');
    expect(state_machine.state.data.syllable_state_machine.state.data.letters).toEqual([]);
    expect(state_machine.state.data.syllable_state_machine.state.data.glyphs).toEqual([]);
    expect(state_machine.initials()).toEqual(['consonant', 'vowel']);
  });

  it('state_machine.process consonant + space (new word)', () => {
    const alphabet = new Devanagari();
    const state_machine = alphabet.state_machine;
    expect(state_machine.state.state).toBe('between');
    expect(state_machine.state.data.words).toEqual([]);
    expect(state_machine.state.data.syllable_state_machine.state.state).toBe('start');
    const consonant = new Letter('consonant', {independent: 'क', latin: 'k'}, 'independent');
    const r1 = state_machine.process(consonant);
    expect(r1).toBe(null);
    const space = new Letter('space', {independent: ' ', latin: ' '}, 'independent');
    const r3 = state_machine.process(space);
    expect(r3).toBe(null);
    expect(state_machine.state.state).toBe('between');
    expect(state_machine.state.data.words).toEqual([
        {
            syllables: [{
                text: 'क',
                letters: [consonant],
                glyphs: ['क']
            }]
        },{
            syllables: []
        }
    ]);
    expect(state_machine.state.data.syllable_state_machine.state.state).toBe('start');
    expect(state_machine.state.data.syllable_state_machine.state.data.text).toBe('');
    expect(state_machine.state.data.syllable_state_machine.state.data.letters).toEqual([]);
    expect(state_machine.state.data.syllable_state_machine.state.data.glyphs).toEqual([]);
    expect(state_machine.initials()).toEqual(['consonant', 'vowel']);
  });

  it('state_machine.process consonant + consonant (new syllable)', () => {
    const alphabet = new Devanagari();
    const state_machine = alphabet.state_machine;
    expect(state_machine.state.state).toBe('between');
    expect(state_machine.state.data.words).toEqual([]);
    expect(state_machine.state.data.syllable_state_machine.state.state).toBe('start');
    const consonant = new Letter('consonant', {independent: 'क', latin: 'k'}, 'independent');
    const r1 = state_machine.process(consonant);
    expect(r1).toBe(null);
    const consonant2 = new Letter('consonant', {independent: 'ख', latin: 'kh'}, 'independent');
    const r3 = state_machine.process(consonant2);
    expect(r3).toBe(null);
    expect(state_machine.state.state).toBe('in_syllable');
    expect(state_machine.state.data.words).toEqual([
        {
            syllables: [{
                text: 'क',
                letters: [consonant, alphabet.a],
                glyphs: ['क', '']
            }]
        }
    ]);
    expect(state_machine.state.data.syllable_state_machine.state.state).toBe('consonant');
    expect(state_machine.state.data.syllable_state_machine.state.data.text).toBe('ख');
    expect(state_machine.state.data.syllable_state_machine.state.data.letters).toEqual([consonant2]);
    expect(state_machine.state.data.syllable_state_machine.state.data.glyphs).toEqual(['ख']);
    expect(state_machine.initials()).toEqual(['closed_restarted', 'closed', 'space', 'vowel', 'nukta', 'virama', 'consonant']);
  });

  it('state_machine.process consonant + vowel + bindu (closed)', () => {
    const alphabet = new Devanagari();
    const state_machine = alphabet.state_machine;
    expect(state_machine.state.state).toBe('between');
    expect(state_machine.state.data.words).toEqual([]);
    expect(state_machine.state.data.syllable_state_machine.state.state).toBe('start');
    const consonant = new Letter('consonant', {independent: 'क', latin: 'k'}, 'independent');
    const r1 = state_machine.process(consonant);
    expect(r1).toBe(null);
    const vowel = new Letter('vowel', {independent: 'आ', dependent: 'ा', latin: 'ā'}, 'independent');
    const r2 = state_machine.process(vowel);
    expect(r2).toBe(null);
    const bindu = new Letter('bindu', {dependent: 'ं'}, 'dependent');
    const r3 = state_machine.process(bindu);
    expect(r3).toBe(null);
    expect(state_machine.state.state).toBe('in_word');
    expect(state_machine.state.data.words).toEqual([{
        syllables: [{
            text: 'कां',
            letters: [consonant, vowel, bindu],
            glyphs: ['क', 'ा', 'ं']
        }]
    }]);
    expect(state_machine.state.data.syllable_state_machine.state.state).toBe('start');
    expect(state_machine.state.data.syllable_state_machine.state.data.text).toBe('');
    expect(state_machine.state.data.syllable_state_machine.state.data.letters).toEqual([]);
    expect(state_machine.state.data.syllable_state_machine.state.data.glyphs).toEqual([]);
    expect(state_machine.initials()).toEqual(['space', 'consonant', 'vowel']);
  });

  it('state_machine.process consonant + consonant + vowel + bindu (closed, 2 syllables)', () => {
    const alphabet = new Devanagari();
    const state_machine = alphabet.state_machine;
    expect(state_machine.state.state).toBe('between');
    expect(state_machine.state.data.words).toEqual([]);
    expect(state_machine.state.data.syllable_state_machine.state.state).toBe('start');
    const consonant = new Letter('consonant', {independent: 'क', latin: 'k'}, 'independent');
    const r0 = state_machine.process(consonant);
    expect(r0).toBe(null);
    const r1 = state_machine.process(consonant);
    expect(r1).toBe(null);
    const vowel = new Letter('vowel', {independent: 'आ', dependent: 'ा', latin: 'ā'}, 'independent');
    const r2 = state_machine.process(vowel);
    expect(r2).toBe(null);
    const bindu = new Letter('bindu', {dependent: 'ं'}, 'dependent');
    const r3 = state_machine.process(bindu);
    expect(r3).toBe(null);
    expect(state_machine.state.state).toBe('in_word');
    expect(state_machine.state.data.words).toEqual([{
        syllables: [{
            text: 'क',
            letters: [consonant, alphabet.a],
            glyphs: ['क', '']
        },{
            text: 'कां',
            letters: [consonant, vowel, bindu],
            glyphs: ['क', 'ा', 'ं']
        }]
    }]);
    expect(state_machine.state.data.syllable_state_machine.state.state).toBe('start');
    expect(state_machine.state.data.syllable_state_machine.state.data.text).toBe('');
    expect(state_machine.state.data.syllable_state_machine.state.data.letters).toEqual([]);
    expect(state_machine.state.data.syllable_state_machine.state.data.glyphs).toEqual([]);
    expect(state_machine.initials()).toEqual(['space', 'consonant', 'vowel']);
  });

  it('state_machine.process consonant + consonant + consonant (2 syllables + ongoing)', () => {
    const alphabet = new Devanagari();
    const state_machine = alphabet.state_machine;
    expect(state_machine.state.state).toBe('between');
    expect(state_machine.state.data.words).toEqual([]);
    expect(state_machine.state.data.syllable_state_machine.state.state).toBe('start');
    const consonant = new Letter('consonant', {independent: 'क', latin: 'k'}, 'independent');
    const r1 = state_machine.process(consonant);
    expect(r1).toBe(null);
    const consonant2 = new Letter('consonant', {independent: 'ख', latin: 'kh'}, 'independent');
    const r2 = state_machine.process(consonant2);
    expect(r2).toBe(null);
    const consonant3 = new Letter('consonant', {independent: 'ग', latin: 'g'}, 'independent');
    const r3 = state_machine.process(consonant3);
    expect(r3).toBe(null);
    expect(state_machine.state.state).toBe('in_syllable');
    expect(state_machine.state.data.words).toEqual([
        {
            syllables: [{
                text: 'क',
                letters: [consonant, alphabet.a],
                glyphs: ['क', '']
            },{
                text: 'ख',
                letters: [consonant2, alphabet.a],
                glyphs: ['ख', '']
            }]
        }
    ]);
    expect(state_machine.state.data.syllable_state_machine.state.state).toBe('consonant');
    expect(state_machine.state.data.syllable_state_machine.state.data.text).toBe('ग');
    expect(state_machine.state.data.syllable_state_machine.state.data.letters).toEqual([consonant3]);
    expect(state_machine.state.data.syllable_state_machine.state.data.glyphs).toEqual(['ग']);
    expect(state_machine.initials()).toEqual(['closed_restarted', 'closed', 'space', 'vowel', 'nukta', 'virama', 'consonant']);
  });

  it('state_machine.process consonant + space + consonant + space (2 words, closed)', () => {
    const alphabet = new Devanagari();
    const state_machine = alphabet.state_machine;
    expect(state_machine.state.state).toBe('between');
    expect(state_machine.state.data.words).toEqual([]);
    expect(state_machine.state.data.syllable_state_machine.state.state).toBe('start');
    const consonant = new Letter('consonant', {independent: 'क', latin: 'k'}, 'independent');
    const r1 = state_machine.process(consonant);
    expect(r1).toBe(null);
    const space = new Letter('space', {independent: ' ', display: 'Space'}, 'independent');
    const r2 = state_machine.process(space);
    expect(r2).toBe(null);
    const consonant2 = new Letter('consonant', {independent: 'ख', latin: 'kh'}, 'independent');
    const r3 = state_machine.process(consonant2);
    expect(r3).toBe(null);
    const r4 = state_machine.process(space);
    expect(r4).toBe(null);
    expect(state_machine.state.state).toBe('between');
    expect(state_machine.state.data.words).toEqual([
        {
            syllables: [{
                text: 'क',
                letters: [consonant],
                glyphs: ['क']
            }]
        },{
           syllables: [{
                text: 'ख',
                letters: [consonant2],
                glyphs: ['ख']
            }] 
        },{
            syllables: []
        }
    ]);
    expect(state_machine.state.data.syllable_state_machine.state.state).toBe('start');
    expect(state_machine.state.data.syllable_state_machine.state.data.text).toBe('');
    expect(state_machine.state.data.syllable_state_machine.state.data.letters).toEqual([]);
    expect(state_machine.state.data.syllable_state_machine.state.data.glyphs).toEqual([]);
    expect(state_machine.initials()).toEqual(['consonant', 'vowel']);
  });

  // ===========================================================================
  // data

  it('state_machine.data consonant + vowel + consonant (restarted)', () => {
    const alphabet = new Devanagari();
    const state_machine = alphabet.state_machine;
    const consonant = new Letter('consonant', {independent: 'स', latin: 's'}, 'independent');
    const r1 = state_machine.process(consonant);
    expect(r1).toBe(null);
    const vowel = new Letter('vowel', {independent: 'आ', dependent: 'ा', latin: 'ā'}, 'independent');
    const r2 = state_machine.process(vowel);
    expect(r2).toBe(null);
    const consonant2 = new Letter('consonant', {independent: 'त', latin: 't'}, 'independent');
    const r3 = state_machine.process(consonant2);
    expect(r3).toBe(null);
    const sm_data = state_machine.data();
    expect(sm_data).toEqual({
        text: 'सा',
        highlight: 'त',
        display_hint: {
            prefix: 'त',
            categories: ['vowel', 'nukta', 'virama']
        }
    });
  });

  it('state_machine.data consonant + virama + consonant (adjuct consonant)', () => {
    const alphabet = new Devanagari();
    const state_machine = alphabet.state_machine;
    const consonant = new Letter('consonant', {independent: 'स', latin: 's'}, 'independent');
    const r1 = state_machine.process(consonant);
    expect(r1).toBe(null);
    const virama = new Letter('virama', {dependent: '्'}, 'dependent');
    const r2 = state_machine.process(virama);
    expect(r2).toBe(null);
    const consonant2 = new Letter('consonant', {independent: 'त', latin: 't'}, 'independent');
    const r3 = state_machine.process(consonant2);
    expect(r3).toBe(null);
    const sm_data = state_machine.data();
    expect(sm_data).toEqual({
        text: '',
        highlight: 'स्त',
        display_hint: {
            prefix: 'स्त',
            categories: ['vowel', 'nukta', 'virama']
        }
    });
  });

  // ===========================================================================
  // initials

  it('state_machine.initials empty', () => {
    const alphabet = new Devanagari();
    const state_machine = alphabet.state_machine;
    const initials = state_machine.initials();
    expect(initials).toEqual(['consonant', 'vowel']);
  })
})
