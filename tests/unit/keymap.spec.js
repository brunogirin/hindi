import KeyMap from '@/components/KeyMap.js'

describe('KeyMap.js', () => {
  it('columnCount', () => {
    const keymap = new KeyMap([[1,2,3],[4,5]]);
    expect(keymap.columnCount).toBe(3);
  });

  it('rowCount', () => {
    const keymap = new KeyMap([[1,2,3],[4,5]]);
    expect(keymap.rowCount).toBe(3);
  });
})
