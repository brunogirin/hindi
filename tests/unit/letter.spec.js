import Letter from '@/components/Letter.js'

describe('Letter.js', () => {
  it('constructor creates letter', () => {
    const letter = new Letter('vowel', {
        independent: "आ",
        dependent: "ा"
    }, 'independent');
    expect(letter.category).toMatch('vowel');
  });

  it('specific glyph can be retrieved', () => {
    const letter = new Letter('vowel', {
        independent: "आ",
        dependent: "ा"
    }, 'independent');
    expect(letter.glyph('dependent')).toMatch('ा');
  });

  it('default glyph can be retrieved', () => {
    const letter = new Letter('vowel', {
        independent: "आ",
        dependent: "ा"
    }, 'independent');
    expect(letter.glyph('default')).toMatch('आ');
  });
})
