import Letter from '@/components/Letter.js'

class KeyMap {
    constructor(mappings) {
        this.mappings = mappings;
        this.mappings.push(
            [new Letter('space', {independent: ' ', display: 'SPACE'}, 'independent')]
        );
    }

    get columnCount() {
        const row_lengths = this.mappings.map(v => v.length);
        return Math.max(...row_lengths);
    }

    get rowCount() {
        return this.mappings.length;
    }
}

export default KeyMap;
