class Letter {
    constructor(category, glyphs, defaultVariant) {
        this.category = category;
        this.glyphs = glyphs;
        this.defaultVariant = defaultVariant;
    }

    glyph(variant) {
        if(variant in this.glyphs) {
            return this.glyphs[variant];
        } else {
            return this.glyphs[this.defaultVariant];
        }
    }

    display_glyph(prefix) {
        if(prefix) {
            return prefix + this.glyph('dependent');
        } else {
            return this.glyph('display');
        }
    }
}

export default Letter;
