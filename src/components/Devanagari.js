import KeyMap from '@/components/KeyMap.js'
import Letter from '@/components/Letter.js'
import StateMachine from '@/components/StateMachine.js'

class Devanagari {
    constructor() {
        this.a = new Letter('vowel', {independent: 'अ', dependent: '', latin: 'a'}, 'independent');
        this.keymap = new KeyMap([
            [
                this.a,
                new Letter('vowel', {independent: 'आ', dependent: 'ा', latin: 'ā'}, 'independent'),
                new Letter('vowel', {independent: 'इ', dependent: 'ि', latin: 'i'}, 'independent'),
                new Letter('vowel', {independent: 'इ', dependent: 'ी', latin: 'ī'}, 'independent'),
                new Letter('vowel', {independent: 'उ', dependent: 'ु', latin: 'u'}, 'independent'),
                new Letter('vowel', {independent: 'ऊ', dependent: 'ू', latin: 'ū'}, 'independent'),
                new Letter('vowel', {independent: 'ए', dependent: 'े', latin: 'e'}, 'independent'),
                new Letter('vowel', {independent: 'ऐ', dependent: 'ै', latin: 'ai'}, 'independent'),
                new Letter('vowel', {independent: 'ओ', dependent: 'ो', latin: 'o'}, 'independent'),
                new Letter('vowel', {independent: 'औ', dependent: 'ौ', latin: 'au'}, 'independent'),
            ],[
                new Letter('consonant', {independent: 'क', latin: 'k'}, 'independent'),
                new Letter('consonant', {independent: 'ख', latin: 'kh'}, 'independent'),
                new Letter('consonant', {independent: 'ग', latin: 'g'}, 'independent'),
                new Letter('consonant', {independent: 'घ', latin: 'gh'}, 'independent'),
                new Letter('consonant', {independent: 'च', latin: 'c'}, 'independent'),
                new Letter('consonant', {independent: 'छ', latin: 'ch'}, 'independent'),
                new Letter('consonant', {independent: 'ज', latin: 'j'}, 'independent'),
                new Letter('consonant', {independent: 'झ', latin: 'jh'}, 'independent'),
                new Letter('bindu', {dependent: 'ँ'}, 'dependent'),
                new Letter('bindu', {dependent: 'ं'}, 'dependent'),
            ],[
                new Letter('consonant', {independent: 'ट', latin: 'ṭ'}, 'independent'),
                new Letter('consonant', {independent: 'ठ', latin: 'ṭh'}, 'independent'),
                new Letter('consonant', {independent: 'ड', latin: 'ḍ'}, 'independent'),
                new Letter('consonant', {independent: 'ढ', latin: 'ḍh'}, 'independent'),
                new Letter('consonant', {independent: 'ण', latin: 'ṇ'}, 'independent'),
                new Letter('consonant', {independent: 'त', latin: 't'}, 'independent'),
                new Letter('consonant', {independent: 'थ', latin: 'th'}, 'independent'),
                new Letter('consonant', {independent: 'द', latin: 'd'}, 'independent'),
                new Letter('consonant', {independent: 'ध', latin: 'dh'}, 'independent'),
                new Letter('consonant', {independent: 'न', latin: 'n'}, 'independent'),
            ], [
                new Letter('consonant', {independent: 'प', latin: 'p'}, 'independent'),
                new Letter('consonant', {independent: 'फ', latin: 'ph'}, 'independent'),
                new Letter('consonant', {independent: 'ब', latin: 'b'}, 'independent'),
                new Letter('consonant', {independent: 'भ', latin: 'bh'}, 'independent'),
                new Letter('consonant', {independent: 'म', latin: 'm'}, 'independent'),
                new Letter('consonant', {independent: 'य', latin: 'y'}, 'independent'),
                new Letter('consonant', {independent: 'र', latin: 'r'}, 'independent'),
                new Letter('consonant', {independent: 'ल', latin: 'l'}, 'independent'),
                new Letter('consonant', {independent: 'व', latin: 'v'}, 'independent'),
                new Letter('consonant', {independent: 'श', latin: 'ś'}, 'independent'),
            ],[
                null,
                new Letter('consonant', {independent: 'ष', latin: 'ṣ'}, 'independent'),
                new Letter('consonant', {independent: 'स', latin: 's'}, 'independent'),
                new Letter('consonant', {independent: 'ह', latin: 'h'}, 'independent'),
                null,
                null,
                new Letter('vowel', {independent: 'ऋ', dependent: 'ृ', latin: 'r̥'}, 'independent'),
                new Letter('nukta', {dependent: '़'}, 'dependent'),
                new Letter('virama', {dependent: '्'}, 'dependent'),
                null,
            ]]);
    // , CS "ङ" "ng"
    // , CS "ञ" "ny"
    // , CS "क़" "q"
    // , CS "ख़" "ḵẖ"
    // , CS "ग़" "g̱"
    // , CS "ज़" "z"
    // , CS "ड़" "ṛ"
    // , CS "ढ़" "ṛh"
    // , CS "फ़" "f"
        this.syllable_state_machine = new StateMachine(() => ({
            state: 'start',
            data: {
                text: "",
                letters: [],
                glyphs: []
            }
        }),
        (state) => {
            let hint = null;
            if(state.state === 'consonant') {
                hint = {
                    prefix: state.data.text,
                    categories: ['vowel', 'nukta', 'virama']
                }
            } else if(state.state === 'vowel') {
                hint = {
                    prefix: state.data.text,
                    categories: ['bindu']
                }
            } else if(state.state === 'nukta') {
                hint = {
                    prefix: state.data.text,
                    categories: ['vowel', 'virama']
                }
            }
            return {
                highlight: state.data.text,
                display_hint: hint
            }
        },
        [{
            from: 'start',
            to: 'consonant',
            categories: ['consonant'],
            update: (letter, state) => {
                const glyph = letter.glyph('independent');
                state.data.text += glyph;
                state.data.letters.push(letter);
                state.data.glyphs.push(glyph);
                return null;
            }
        },{
            from: 'start',
            to: 'vowel',
            categories: ['vowel'],
            update: (letter, state) => {
                const glyph = letter.glyph('independent');
                state.data.text += glyph;
                state.data.letters.push(letter);
                state.data.glyphs.push(glyph);
                return null;
            }
        },{
            from: 'consonant',
            to: 'vowel',
            categories: ['vowel'],
            update: (letter, state) => {
                const glyph = letter.glyph('dependent');
                state.data.text += glyph;
                state.data.letters.push(letter);
                state.data.glyphs.push(glyph);
                return null;
            }
        },{
            from: 'consonant',
            to: 'nukta',
            categories: ['nukta'],
            update: (letter, state) => {
                const glyph = letter.glyph('dependent');
                state.data.text += glyph;
                state.data.letters.push(letter);
                state.data.glyphs.push(glyph);
                return null;
            }
        },{
            from: 'consonant',
            to: 'virama',
            categories: ['virama'],
            update: (letter, state) => {
                const glyph = letter.glyph('dependent');
                state.data.text += glyph;
                state.data.letters.push(letter);
                state.data.glyphs.push(glyph);
                return null;
            }
        },{
            from: 'consonant',
            to: 'end',
            categories: ['consonant'],
            update: (letter, state) => {
                const glyph = this.a.glyph('dependent');
                state.data.text += glyph;
                state.data.letters.push(this.a);
                state.data.glyphs.push(glyph);
                return {
                    category: 'closed_restarted',
                    syllable: JSON.parse(JSON.stringify(state.data)),
                    letter: letter
                };
            }
        },{
            from: 'virama',
            to: 'consonant',
            categories: ['consonant'],
            update: (letter, state) => {
                const glyph = letter.glyph('independent');
                state.data.text += glyph;
                state.data.letters.push(letter);
                state.data.glyphs.push(glyph);
                return null;
            }
        },{
            from: 'virama',
            to: 'end',
            categories: ['vowel'],
            update: (letter, state) => {
                return {
                    category: 'closed_restarted',
                    syllable: JSON.parse(JSON.stringify(state.data)),
                    letter: letter
                };
            }
        },{
            from: 'nukta',
            to: 'vowel',
            categories: ['vowel'],
            update: (letter, state) => {
                const glyph = letter.glyph('dependent');
                state.data.text += glyph;
                state.data.letters.push(letter);
                state.data.glyphs.push(glyph);
                return null;
            }
        },{
            from: 'nukta',
            to: 'virama',
            categories: ['virama'],
            update: (letter, state) => {
                const glyph = letter.glyph('dependent');
                state.data.text += glyph;
                state.data.letters.push(letter);
                state.data.glyphs.push(glyph);
                return null;
            }
        },{
            from: 'nukta',
            to: 'end',
            categories: ['consonant'],
            update: (letter, state) => {
                const glyph = this.a.glyph('dependent');
                state.data.text += glyph;
                state.data.letters.push(this.a);
                state.data.glyphs.push(glyph);
                return {
                    category: 'closed_restarted',
                    syllable: JSON.parse(JSON.stringify(state.data)),
                    letter: letter
                };
            }
        },{
            from: 'vowel',
            to: 'end',
            categories: ['bindu'],
            update: (letter, state) => {
                const glyph = letter.glyph('dependent');
                state.data.text += glyph;
                state.data.letters.push(letter);
                state.data.glyphs.push(glyph);
                return {
                    category: 'closed',
                    syllable: JSON.parse(JSON.stringify(state.data))
                };
            }
        },{
            from: 'vowel',
            to: 'end',
            categories: ['consonant', 'vowel'],
            update: (letter, state) => {
                return {
                    category: 'closed_restarted',
                    syllable: JSON.parse(JSON.stringify(state.data)),
                    letter: letter
                };
            }
        }]);
        this.state_machine = new StateMachine(
            () => ({
                state: 'between',
                data: {
                    words: [],
                    syllable_state_machine: this.syllable_state_machine
                }
            }),
            (state) => {
                const syllable_data = state.data.syllable_state_machine.data();
                return {
                    text: state.data.words.map(
                            w => w.syllables.map(s => s.text).join('')
                        ).join(' '),
                    highlight: syllable_data.highlight,
                    display_hint: syllable_data.display_hint
                }
            },
            [{
                from: 'between',
                to: 'in_syllable',
                categories: ['*'],
                update: (letter, state) => {
                    return state.data.syllable_state_machine.process(letter);
                },
                initials: (state) => state.data.syllable_state_machine.initials()
            },{
                from: 'in_syllable',
                to: 'in_syllable',
                categories: ['*'],
                update: (letter, state) => {
                    return state.data.syllable_state_machine.process(letter);
                },
                initials: (state) => state.data.syllable_state_machine.initials()
            },{
                from: 'in_syllable',
                to: 'in_syllable',
                categories: ['closed_restarted'],
                update: (event, state) => {
                    if(state.data.words.length > 0) {
                        const last_word = state.data.words[state.data.words.length - 1];
                        last_word.syllables.push(event.syllable);
                    } else {
                        state.data.words.push({
                            syllables: [event.syllable]
                        });
                    }
                    state.data.syllable_state_machine.restart();
                    return state.data.syllable_state_machine.process(event.letter);
                }
            },{
                from: 'in_syllable',
                to: 'in_word',
                categories: ['closed'],
                update: (event, state) => {
                    if(state.data.words.length > 0) {
                        const last_word = state.data.words[state.data.words.length - 1];
                        last_word.syllables.push(event.syllable);
                    } else {
                        state.data.words.push({
                            syllables: [event.syllable]
                        });
                    }
                    state.data.syllable_state_machine.restart();
                }
            },{
                from: 'in_syllable',
                to: 'between',
                categories: ['space'],
                update: (letter, state) => {
                    const syllable = JSON.parse(JSON.stringify(
                        state.data.syllable_state_machine.state.data));
                    if(state.data.words.length > 0) {
                        const last_word = state.data.words[state.data.words.length - 1];
                        last_word.syllables.push(syllable);
                    } else {
                        state.data.words.push({
                            syllables: [syllable]
                        });
                    }
                    // insert empty word for upcoming syllables
                    state.data.words.push({
                        syllables: []
                    });
                    state.data.syllable_state_machine.restart();
                }
            },{
                from: 'in_word',
                to: 'between',
                categories: ['space'],
                update: (letter, state) => {
                    state.data.words.push({
                        syllables: []
                    });
                }
            },{
                from: 'in_word',
                to: 'in_syllable',
                categories: ['*'],
                update: (letter, state) => {
                    return state.data.syllable_state_machine.process(letter);
                },
                initials: (state) => state.data.syllable_state_machine.initials()
            }]
        );
    }
}

export default Devanagari;
