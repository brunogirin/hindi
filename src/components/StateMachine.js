class StateMachine {
    constructor(init, build_data, transitions) {
        this.init = init;
        this.state = this.init();
        this.transition_map = {};
        transitions.forEach(t => {
            const map_entry = this.transition_map[t.from] || {
                categories: {},
                default: null
            }
            t.categories.forEach(c => {
                if(c === '*') {
                    map_entry.default = t;
                } else {
                    map_entry.categories[c] = t;
                }
            })
            this.transition_map[t.from] = map_entry
        });
        this.build_data = build_data;
    }

    find_transition(letter) {
        let transition = null;
        if(this.state.state in this.transition_map) {
            const possible = this.transition_map[this.state.state]
            if(letter.category in possible.categories) {
                transition = possible.categories[letter.category]
            } else {
                transition = possible.default
            }
        }
        return transition;
    }

    process_transition(letter, transition) {
        const next = (transition.update && transition.update(letter, this.state)) || null;
        this.state.state = transition.to;
        return next;
    }

    process(letter) {
        const transition = this.find_transition(letter);
        if(transition) {
            const next = this.process_transition(letter, transition)
            if(next && this.find_transition(next)) {
                return this.process(next);
            } else {
                return next
            }
        } else {
            throw `Invalid category ${letter.category} for state ${this.state.state}`;
        }
    }

    restart() {
        this.state = this.init();
    }

    data() {
        return this.build_data(this.state);
    }

    initials() {
        if(this.state.state in this.transition_map) {
            const entry = this.transition_map[this.state.state];
            let initials = Object.keys(entry.categories);
            if(entry.default) {
                if(entry.default.initials) {
                    initials = initials.concat(entry.default.initials(this.state));
                } else {
                    initials.push('*');
                }
            }
            return initials;
        } else {
            return [];
        }
    }
}

export default StateMachine;
