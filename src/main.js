import Vue from 'vue'
import App from './App.vue'

Vue.config.productionTip = false

new Vue({
  render: h => h(App),
  mounted: function() {
    document.title = 'Devanagari Keyboard';
  }
}).$mount('#app')
